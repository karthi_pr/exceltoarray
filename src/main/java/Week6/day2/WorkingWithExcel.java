package Week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class WorkingWithExcel
{
	@Test
	public void importExcel() throws IOException
	{

		XSSFWorkbook wb=new XSSFWorkbook("./data/topManagement.xlsx");
		XSSFSheet sheet=wb.getSheetAt(0);
		int rowCount=sheet.getLastRowNum();
		System.out.println(rowCount);
		int cellCount=sheet.getRow(0).getLastCellNum();
		System.out.println(cellCount);
		for(int i=1;i<=rowCount;i++)
		{
			XSSFRow row=sheet.getRow(i);
			for(int j=0;j<cellCount;j++)
			{
				XSSFCell cell=row.getCell(j);

				String value=cell.getStringCellValue();
				System.out.print(value+" ");

			}
			System.out.println();
			wb.close();
		}

	}
}
